-- Create schema reactdb
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ reactdb;
USE reactdb;

--
-- Table structure for table `reactdb`.`users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipoid` varchar(4) DEFAULT NULL,
  `documento` varchar(20) DEFAULT NULL,
  `nombre1` varchar(30) DEFAULT NULL,
  `nombre2` varchar(30) DEFAULT NULL,
  `apellido1` varchar(30) DEFAULT NULL,
  `apellido2` varchar(30) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `pais` varchar(30) DEFAULT NULL,
  `area` varchar(50) DEFAULT NULL,
  `estado` varchar(10) DEFAULT 'ACTIVO',
  `creacion` varchar(30) NOT NULL,
  `fchedicion` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reactdb`.`users`
--

